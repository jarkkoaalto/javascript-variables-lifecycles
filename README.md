# Javascript-variables-lifecycles

About this course
Understand JavaScript Engine phases and lifecycles of the &#34;var&#34;, &#34;let&#34;, &#34;const&#34;. Scopes, hoisting, closures simplified.

Course content
##### Section 02: Scopes
##### Section 03: Var, Let and Const
##### Section 04: JavaScript Engine Phases